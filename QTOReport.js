
var record = 0;
var startConnectionRecord = 0;
var endConnectionRecord = 0;

function ApplyGroupbyBasedOnFilter(jsonDetails) {
    record = 0;
    //Form filter
    var fields = [];

    $.each(jsonDetails.filters, function (name, value) {
        fields.push({ "field": name })
    });

    var dataSource = new kendo.data.DataSource({
        data: jsonDetails.Data,
        group: fields,
        pageSize: 20,
    });

    var grid = $("#QTOKendoGrid").data("kendoGrid");
    grid.setDataSource(dataSource);

}


function BuildQTOReportKendoGrid() {
    $("#QTOKendoGrid").kendoGrid({
        dataSource: {
            data: [],
            schema: {
                model: {
                    fields: {
                        SLNo: { type: "number" },
                        MemberType: { type: "string" },
                        MemberSubType: { type: "string" },
                        EndConnections: { type: "string" },
                        Profile: { type: "string" },
                        Sequence: { type: "string" },
                        Length: { type: "string" },
                        Grade: { type: "string" },
                        Quantity: { type: "number" },
                        Weight: { type: "string" },
                        Area: { type: "string" },
                        Shape: { type: "string" },
                        CVN: { type: "string" },
                        Camber: { type: "string" },
                        ShearStuds: { type: "number" },
                        Finish: { type: "string" },
                        Remarks: { type: "string" }
                    }
                }
            },
            pageSize: 20
        },
        columnMenu: {
            filterable: true
        },
        height: window.innerHeight - 30,
        sortable: true,
        navigatable: true,
        resizable: true,
        reorderable: true,
        groupable: true,
        filterable: true,
        detailInit: detailInit,
        columns: [
            {
                title: "Sl.No",
                template: "#= ++record #",
                width: 75,
            },
            {
                field: "MemberType",
                title: "Member Type",
                width: 150,
            },
            {
                field: "MemberSubType",
                title: "Member Sub-Type",
                width: 150,
            },
            {
                title: "End Connections",
                width: 200,
                template: function (dataItem) {
                    return dataItem.ConnectionType.replaceAll("_"," ") ;
                },
            },
            {
                field: "Profile",
                title: "Profile",
                width: 100,
            },
            {
                field: "Sequence",
                title: "Sequence",
                width: 100,
            },
            {
                field: "Length",
                title: "Length (Ft-in)",
                width: 100,
            },
            {
                field: "Grade",
                title: "Grade",
                width: 100,
            },
            {
                field: "Quantity",
                title: "Quantity",
                width: 100,
            },
            {
                field: "Weight",
                title: "Weight (lbs)",
                width: 100,
            },
            {
                field: "Area",
                title: "Area (Sft)",
                width: 100,
            },
            {
                field: "Shape",
                title: "Shape",
                width: 100,
            },
            {
                field: "CVN",
                title: "CVN",
                width: 100,
            },
            {
                field: "Camber",
                title: "Camber",
                width: 100,
            },
            {
                field: "ShearStuds",
                title: "Shear Studs",
                width: 100, 
            },
            {
                field: "Finish",
                title: "Finish",
                template: function (dataItem) {
                    return generateFinishColumnTemplate(dataItem, "#= ++record #");
                },
                width: 100, 
            },
            {
                field: "Remarks",
                title: "Remarks",
                width: 100, 
            },
        ],
        pageable: {
            pageSizes: [20, 50, 100, 500],
            numeric: true
        },
        dataBound: function () {

            record = 0;

            var grid = $("#QTOKendoGrid").data("kendoGrid");

            var gridRows = grid.tbody.find("tr");

            gridRows.each(function (e) {
                var rowItem = grid.dataItem($(this));
                if ($(this).find('.viewbtn').length) {
                    $(this).find('.viewbtn').kendoTooltip({
                        position: "right",
                        height: "200px",
                        width: "300px",
                        showOn: "click",
                        autoHide: true,
                        content: function () {


                            return "<table>" +
                                "<tr class ='viewbtnHeader'>" +
                                "<td>Surface Preparaton </br><b>" + rowItem.Finish.SurfacePreparation + "</b></td>" +
                                "<td></td>" +
                                "</tr>" +
                                "<tr class ='viewbtnHeader'>" +
                                "<td>Primer Name </br><b>" + rowItem.Finish.PrimerName + "</b></td>" +
                                "<td>No. of coats </br><b>" + rowItem.Finish.NoOfCoats + "</b></td>" +
                                "</tr>" +
                                "<tr class ='viewbtnHeader'>" +
                                "<td>Finish Type </br><b>" + rowItem.Finish.FinshTpe + "</b></td>" +
                                "<td></td>" +
                                "</tr>" +
                                "<tr class ='viewbtnHeader'>" +
                                "<td>Zinc coating Thickness </br><b>" + rowItem.Finish.ZincCoatingThickness + "</b></td>" +
                                "<td></td>" +
                                "</tr>" +
                                "<tr class ='viewbtnHeader'>" +
                                "<td>AESS Category </br><b>" + rowItem.Finish.AESSCategory + "</b></td>" +
                                "<td></td>" +
                                "</tr>" +
                                "</table>";

                        },
                    });
                }

            });
        },
    });
}

function generateFinishColumnTemplate(dataItem) {
    return "<button class='viewbtn'>view</button>";
}

function detailInit(e) {

    startConnectionRecord = 0;
    endConnectionRecord = 0;
    console.log(e);
    if (e.data.StartConnectionDetails.length > 0) {
        childgridConnection(e,e.data.StartConnectionType,e.data.StartConnectionDetails);
    }

    if (e.data.EndConnectionDetails.length > 0) {
        childgridConnection(e,e.data.EndConnectionType,e.data.EndConnectionDetails);
    }
}

function childgridConnection(e,connectionType,connectionDetails) {

    console.log(e);
    $("".concat("<div><div class='connectionHeader'>", connectionType.replace("_"," "), "</b></div>")).appendTo(e.detailCell).kendoGrid({
        dataSource: {
            data: BuildChildRow(connectionDetails),
            schema: {
                model: {
                    fields: {
                        SLNo: { type: "number" },
                        ConnectionMaterialsProfile: { type: "string" },
                        ConnectionMaterialsGrade: { type: "string" },
                        ConnectionMaterialsQuantity: { type: "number" },
                        ConnectionMaterialsWeight: { type: "number" },
                        ConnectionMaterialsLength: { type: "string" },
                        BoltDetailsDia: { type: "string" },
                        BoltDetailsGrade: { type: "string" },
                        BoltDetailsQuantity: { type: "number" },
                        BoltDetailsLength: { type: "string" },
                        BoltDetailsBoltType: { type: "string" },
                        WeldDetailsType: { type: "string" },
                        WeldDetailsSize: { type: "string" },
                        WeldDetailsQuantity: { type: "string" },
                        WeldDetailsLength: { type: "string" },
                        WeldDetailsweldType: { type: "string" },
                        HoleDetailsType: { type: "string" },
                        HoleDetailsDia: { type: "string" },
                        HoleDetailsQuantity: { type: "number" },
                        Remarks: { type: "string" }
                    }
                }
            },
        },
        scrollable: false,
        sortable: true,
        pageable: false,
        columns: [
            {
                title: "Sl.No",
                template: "#= ++startConnectionRecord #",
            },
            {
                title: "Connection Materials",
                columns: [
                    {
                        field: "Profile", title: "Profile",
                        width: 150,
                    },
                    {
                        field: "Grade", title: "Grade",
                        width: 100,
                    },
                    {
                        field: "Quantity", title: "Quantity",
                        width: 100,
                    },
                    {
                        field: "Weight", title: "Weight",
                        width: 100,
                    },
                    {
                        field: "Length", title: "Length",
                        width: 100,
                    },
                ]
            },
            {
                title: "Bolt Details",
                columns: [
                    {
                        title: "Dia",
                        template: function (dataItem) {
                            return RowTemplate(dataItem.BoltDia);
                        },
                        width: 100
                    },
                    {
                        title: "Grade",
                        template: function (dataItem) {
                            return RowTemplate(dataItem.BoltGrade);
                        },
                        width: 100
                    },
                    {
                        title: "Quantity",
                        template: function (dataItem) {
                            return RowTemplate(dataItem.BoltQuantity);
                        },
                        width: 100
                    },
                    {
                        title: "Length",
                        template: function (dataItem) {
                            return RowTemplate(dataItem.BoltLength);
                        },
                        width: 100
                    },
                    {
                        title: "Bolt Type",
                        template: function (dataItem) {
                            return RowTemplate(dataItem.BoltType);
                        },
                        width: 100
                    },
                ]
            },
            {
                title: "Weld Details",
                columns: [
                    {
                        title: "Type",
                        template: function (dataItem) {
                            return RowTemplate(dataItem.WeldType);
                        },
                        width: 150
                    },
                    {
                        title: "Size",
                        template: function (dataItem) {
                            return RowTemplate(dataItem.WeldSize);
                        },
                        width: 100
                    },
                    {
                        title: "Quantity",
                        template: function (dataItem) {
                            return RowTemplate(dataItem.WeldQuantity);
                        },
                        width: 100
                    },
                    {
                        title: "Length",
                        template: function (dataItem) {
                            return RowTemplate(dataItem.WeldLength);
                        },
                        width: 100
                    },
                    {
                        title: "Shop / Site",
                        template: function (dataItem) {
                            return RowTemplate(dataItem.WeldShopSite);
                        },
                        width: 100
                    },
                ]
            },
            {
                title: "Hole Details",
                columns: [
                    {
                        title: "Type",
                        template: function (dataItem) {
                            return RowTemplate(dataItem.HoleType);
                        },
                        width: 100
                    },
                    {
                        title: "Dia",
                        template: function (dataItem) {
                            return RowTemplate(dataItem.HoleDia);
                        },
                        width: 100
                    },
                    {
                        title: "Quantity",
                        template: function (dataItem) {
                            return RowTemplate(dataItem.HoleQuantity);
                        },
                        width: 100
                    }
                ]
            },
            {
                field: "Remarks",
                title: "Remarks",
            }
        ],
    });
}

function BuildChildRow(sourceJson) {
    var data = [];
    if (sourceJson) {

        $.each(sourceJson, function (index, item) {
            var boltDetails = GetBoltDetails(item.BoltDetails);
            var weldDetails = GetWeldDetails(item.WeldDetails);
            var holeDetails = GetHoleDetails(item.HoleDetails);
            var row = {
                SNo: index + 1,
                Profile: item.Profile,
                Grade: item.Grade,
                Weight: item.Weight,
                Quantity: item.Quantity,
                Length: item.Length,
                BoltDia: boltDetails.DiaRows,
                BoltGrade: boltDetails.GradeRows,
                BoltQuantity: boltDetails.QuantityRows,
                BoltLength: boltDetails.LengthRows,
                BoltType: boltDetails.BoltTypeRows,
                WeldType: weldDetails.TypeRows,
                WeldSize: weldDetails.SizeRows,
                WeldQuantity: weldDetails.QuantityRows,
                WeldLength: weldDetails.LengthRows,
                WeldShopSite: weldDetails.ShopSiteRows,
                HoleType: holeDetails.TypeRows,
                HoleDia: holeDetails.DiaRows,
                HoleQuantity: holeDetails.QuantityRows
            }
            data.push(row);

        });
    }
    return data;
}

function GetBoltDetails(boltArray) {
    var BoltRows = {
        DiaRows: [],
        GradeRows: [],
        QuantityRows: [],
        LengthRows: [],
        BoltTypeRows: []
    }
    boltArray.forEach(function (item) {
        BoltRows.DiaRows.push(item.Dia);
        BoltRows.GradeRows.push(item.Grade);
        BoltRows.QuantityRows.push(item.Quantity);
        BoltRows.LengthRows.push(item.Length);
        BoltRows.BoltTypeRows.push(item.BoltType);
    });
    return BoltRows;
}

function GetWeldDetails(weldArray) {
    var WeldRows = {
        TypeRows: [],
        SizeRows: [],
        QuantityRows: [],
        LengthRows: [],
        ShopSiteRows: []
    }
    weldArray.forEach(function (item) {
        WeldRows.TypeRows.push(item.Type);
        WeldRows.SizeRows.push(item.Size);
        WeldRows.QuantityRows.push(item.Quantity);
        WeldRows.LengthRows.push(item.Length);
        WeldRows.ShopSiteRows.push(item.ShopSite);
    });
    return WeldRows;
}

function GetHoleDetails(holeArray) {
    var HoleRows = {
        TypeRows: [],
        DiaRows: [],
        QuantityRows: []
    }
    holeArray.forEach(function (item) {
        HoleRows.TypeRows.push(item.Type);
        HoleRows.DiaRows.push(item.Dia);
        HoleRows.QuantityRows.push(item.Quantity);
    });
    return HoleRows;
}

function RowTemplate(data) {

    if (data.length) {

        var rowClass = "rowstyle";
        var divCode = "";
        data.forEach(function (rowObj) {

            divCode += "<div class='" + rowClass + " tworowheight'><div class='column onetd'>";
            divCode += "<span class='beamSpan'>".concat(rowObj) + "</span>";
            divCode += "</div></div>";
            rowClass = "secondrow";

        });

        return divCode;
    }
    return "";

}
